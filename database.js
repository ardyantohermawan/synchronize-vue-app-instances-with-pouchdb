/**
 * Cloudant credentials and database name
 */
const username = 'your-cloudant-user-name'
const password = 'your-cloudant-password'
const dbName = 'cloud-chat'

/**
 * Chat database URL
 */
const dbUrl = `https://${username}:${password}@${username}.cloudantnosqldb.appdomain.cloud/${dbName}`

/**
 * Local database connection
 */
let local = null
/**
 * Remote database connection
 */
let remote = null


/**
 * Connects to local database and initiates synchronization with the remote database
 * @param onConnected Callback triggered when database is connected
 * @param onChanged Callback triggered when database reports changes
 * @param onError Callback triggered when database reports errors
 */
export function connect ({ onConnected, onChanged, onError } = {}) {
  remote = new PouchDB(dbUrl)
  local = new PouchDB(dbName)
  local
    .sync(remote, { live: true, retry: true })
    .on('change', changes => onChanged ? onChanged(changes) : undefined)
    .on('error', error => onError ? onError(error) : undefined)
  if (onConnected) {
    onConnected()
  }
}

/**
 * Loads chat room content from the local database
 */
export async function loadMessages () {
  const data = await local.allDocs({
    include_docs: true,
    startkey: 'message:'
  })
  const messages = data.rows.map(row => row.doc)
  messages.sort((a, b) => a.timestamp > b.timestamp ? 1 : -1)
  return messages
}

/**
 * Saves a chat message to the local database
 * @param sender Message sender
 * @param text Message text
 */
export function saveMessage (sender, text) {
  if (!(sender && text)) return

  const timestamp = new Date().getTime()
  const _id = 'message:' + sender + '-' + timestamp
  const message = {
    _id,
    timestamp,
    sender,
    text
  }
  return local.put(message)
}
